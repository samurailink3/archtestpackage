# Arch Linux Test Git Package

This project is just a test of git checkout, C compilation, Makefiles, PKGBUILD
files, and package signing.

# License

This is all public domain. Do what you want, I don't need credit or
notification. This is all for the purpose of learning. Build, distribute, sell,
copy, remix, destroy, hate, love, whatever.

# Explanation

## hello.c

This is just "Hello, World!" in C. Nothing special.

## Makefile

`all:` in the makefile will run with just `make`. It compiles `hello.c` into the
executable `hello.out`.

`install:` in the makefile will run with `make install`. This takes the optional
argument `DESTDIR`. `DESTDIR` will copy the `hello.out` binary to a prefix you
specify. By default the application will deploy `hello.out` to `/usr/bin/hello`,
by using `make DESTDIR=/home/myuser/bin/ install`, `make` will deploy the
application to `/home/user/bin/usr/bin/hello`. This is useful for `PKGBUILD`
files specifically, and a host of other applications.

`uninstall:` in the make file will run with `make uninstall`. It removes the
copied executable from `$DESTDIR/usr/bin/hello`.

## PKGBUILD

This `PKGBUILD` file appears to be the bare minimum needed to pull, compile, and
package a git repository.

* `pkgname` - The name of the package itself and the name of the package in the repo.
* `pkgdesc` - The description of the package.
* `license` -
* `arch` -
* `_source` -
* `_srcname` -
* `pkgver` - The version of the package. Is usually set by
* `pkgrel`
* `makedepends` -
* `md5sums` -

### `build()`

### `package()`
