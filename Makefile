all:
		gcc hello.c -o hello.out
install:
		mkdir -p ${DESTDIR}/usr/bin/
		cp hello.out ${DESTDIR}/usr/bin/hello
uninstall:
		rm ${DESTDIR}/usr/bin/hello
